package com.qaagility.controller;

public class Count12 {

    public int giorkaros(int protos, int deuteros) {
        if (deuteros == 0)
            return Integer.MAX_VALUE;
        else
            return protos / deuteros;
    }

}
